export const maxImageSize = 1048576;
export const maxWidth = 800;
export const maxHeight = 100;
export const cloudinaryConfig = {
  uploadUrl: 'https://api.cloudinary.com/v1_1/wfdns6x2g6/image/upload',
  uploadPreset: 'zccdzswb',
};
