import { cloudinaryConfig } from '../config';

export default (imageBlob) => {
  const formData = new FormData();
  formData.append('file', imageBlob);
  formData.append('upload_preset', cloudinaryConfig.uploadPreset);
  return fetch(cloudinaryConfig.uploadUrl, {
    method: 'POST',
    body: formData,
    withCredentials: false,
  })
    .then(response => response.json())
    .then(({ secure_url: secureURL }) => secureURL);
};
