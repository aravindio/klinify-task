import React from 'react';
import ReactImageCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import uploadImageToCloud from '../api';
import { maxImageSize, maxWidth, maxHeight } from '../config';

/*
 * Since `newWindowContent` is being used only once, I can directly use
 * it where I want instead of storing it in a variable.
 */
const newWindowContent = imageUrl => `
  <!DOCTYPE html>
  <html>
    <head>
      <title>Print Preview</title>
      <style>
        * {
          box-sizing: border-box;
        }
        html,
        body,
        div {
          height: 100%;
          width: 100%;
          margin: 0;
          padding: 10px;
        }
        div {
          background: white;
          text-align: center;
        }
      </style>
    </head>
    <body>
      <div>
        <img src=${imageUrl} alt="Uploaded content" />
      </div>
      <script>
        setTimeout(() => {
          newWindow.print();
          newWindow.close();
        }, 3000);
      </script>
    </body>
  </html>
`;

class App extends React.Component {
  state = {
    image: null,
    /*
     * Instead of using `imagePreviewUrl`, I can completely skip it and use `image.src`
     * and test for `!image` instead of `!imagePreviewUrl` inside `render` method.
     * `imagePreviewUrl is redundant.
     */
    notification: '',
    crop: {},
    isUploadInProgress: false,
  };

  getCroppedImage = () => {
    const { image, crop } = this.state;
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
    const croppedImageWidth = (crop.width * image.naturalWidth) / 100;
    const croppedImageHeight = (crop.height * image.naturalHeight) / 100;
    canvas.width = croppedImageWidth;
    canvas.height = croppedImageHeight;
    context.drawImage(
      image,
      (crop.x * image.naturalWidth) / 100,
      (crop.y * image.naturalHeight) / 100,
      croppedImageWidth,
      croppedImageHeight,
      0,
      0,
      croppedImageWidth,
      croppedImageHeight,
    );
    /*
     * As per "react-image-crop" documentation, I can pass 'image/jpeg'
     * as a second argument to `toBlob` function to optimize the image
     * conversion speed.
     */
    return new Promise(resolve => canvas.toBlob(file => resolve(file)));
  };

  handleFormSubmit = event => event.preventDefault();

  handleImageUpload = (event) => {
    event.preventDefault();
    const file = event.target.files[0];
    const image = new Image();
    const URL = window.URL || window.webkitURL;
    const self = this;
    image.onload = function onImageLoad() {
      if (file.size <= maxImageSize) {
        self.setState({ image: this });
      } else {
        self.setState({ notification: 'File size cannot be more than 1MB.' });
      }
    };
    image.src = URL.createObjectURL(file);
  };

  // I should set image and crop to null on reset.
  handleReset = () => this.setState({ image: null, crop: {}, notification: '' });

  handleCrop = crop => this.setState({ crop });

  handlePrintPreviewClick = async () => {
    const { image } = this.state;
    this.setState({ isUploadInProgress: true });
    const imageBlob = await this.getCroppedImage();
    uploadImageToCloud(imageBlob || image)
      .then((imageUrl) => {
        const newWindow = window.open('', '_blank');
        newWindow.newWindow = newWindow;
        newWindow.document.write(newWindowContent(imageUrl));
        this.setState({ crop: {}, isUploadInProgress: false }, this.handleReset);
      });
  };

  render() {
    const { notification, crop, image, isUploadInProgress } = this.state;
    return (
      <div>
        <h1>{!image ? 'Upload an image' : 'Crop the image'}</h1>
        {
          !!notification && <h4>{notification}</h4>
        }
        {
          image && (
            <div className="image-preview">
              {
                /*
                 * Instead of disabling the preview button when there is no
                 * image, I'm only showing the preview button when there is an image
                 * and hiding it if there is no image.
                 */
              }
              {
                /*
                 * Here, maxWidth and maxHeight aren't actually enforcing the
                 * width & height. According to "react-image-crop" documentation,
                 * they are being passed as a percentage of the image.
                 * I should pass the following for maxWidth and maxHeight props instead
                 *
                 * maxWidth: (maxWidth / image.naturalWidth) * 100
                 * maxHeight: (maxHeight / image.naturalHeight) * 100
                 *
                 * Doing so should enforce proper dimensions.
                 */
               }
              <ReactImageCrop
                maxWidth={(maxWidth / image.naturalWidth) * 100}
                maxHeight={(maxHeight / image.naturalHeight) * 100}
                src={image.src}
                crop={crop}
                onChange={this.handleCrop}
              />
            </div>
          )
        }
        {
          // Form tag and this.handleFormSubmit are redundant
        }
        {
          !image && (
            <input
              id="file-upload"
              type="file"
              accept="image/x-png, image/gif, image/jpeg"
              onChange={this.handleImageUpload}
            />
          )
        }
        {
          // Since no image is a valid value, I can remove the `disbled` prop.
        }
        <div>
          <input
            className="reset-button"
            type="reset"
            onClick={this.handleReset}
          />
          <button
            className="preview-button"
            onClick={this.handlePrintPreviewClick}
            disabled={isUploadInProgress || !image}
          >
            {isUploadInProgress ? 'Uploading...' : 'Print Preview'}
          </button>
        </div>
      </div>
    );
  }
}

export default App;
